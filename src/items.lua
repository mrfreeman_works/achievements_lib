local S = minetest.get_translator("achievements_lib")

minetest.register_craftitem("achievements_lib:achievements_menu", {
  inventory_image = "achievements_lib_item.png",
  description = S("Achievements"),
  on_place = function() end,
  on_drop = function() end,


  on_use = function(itemstack, player, pointed_thing)
    if not player then return end
    achvmt_lib.gui_show(player:get_player_name())
  end
})
