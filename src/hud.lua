local S = minetest.get_translator("achievements_lib")



function achvmt_lib.hud_create(p_name)
  Panel:new("achvmts", {
    player = p_name,
    position  = {x = 0.5, y = 1},
    offset = {x = 0, y = -130},
    bg_scale = {x = 2, y = 2},
    bg = "achvmtlib_hud_bg.png",
    title = "",
    visible = false,
    z_index = 9999,
    sub_txt_elems = {
      title = {
        size    = { x = 1 },
        offset  = { x =28, y = -11 },
        number  = "0xf4b41b",
        text    = ""
      },
      description = {
        size    = { x = 1 },
        offset  = { x = 28, y = 13 },
        number  = "0xd7ded7",
        text    = ""
      }
    },
    sub_img_elems = {
      icon = {
        offset = {x = -183, y = 1},
        text = ""
      }
    }
  })
end



function achvmt_lib.hud_show(p_name, ach_def)
  local panel = panel_lib.get_panel(p_name, "achvmts")

  panel:update(nil, {title = { text = ach_def.title }, description = { text = ach_def.description }}, {icon = { text = ach_def.image}})
  panel:show()

  minetest.sound_play("achievements_lib_achievement", {to_player = p_name})
  minetest.chat_send_player(p_name, minetest.colorize("#a0938e", "[" .. S("Achievement Unlocked") .. "] " .. ach_def.title))

  minetest.after(5, function()
    if not minetest.get_player_by_name(p_name) then return end
    panel:hide()
  end)
end
