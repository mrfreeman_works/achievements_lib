local S = minetest.get_translator("achievements_lib")

-- show fs command
minetest.register_chatcommand("achievements", {
  description = S("See your achievements"),
  func = function(p_name, param)
    local player = minetest.get_player_by_name(p_name)
    if player then achvmt_lib.gui_show(p_name) end
  end
})



ChatCmdBuilder.new("achvmt_lib", function(cmd)
  cmd:sub("award :player :achname", function(sender, p_name, ach_name)
    return achvmt_lib.award(p_name, ach_name)
  end)

  cmd:sub("unaward :player :achname", function(name, p_name, ach_name)
    if ach_name == "all" then
      return achvmt_lib.unaward_all(p_name)
    else
      return achvmt_lib.unaward(p_name, ach_name)
    end
  end)

end, {
  description = [[
    (/help achvmt_lib)
    Use this to award or revoke achievements:
    - award <p_name> <achievement_name> -- awards an achievement to a player
    - unaward <p_name> <achievement_name> -- removes an achievement from a player
    - unaward <p_name> all -- removes all achievements from a player
    ]],
  privs = {achvmt_lib_admin = true}
})
